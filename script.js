$(document).ready(

  function Modal() {

    $('.detail').on("click", function(){
      $('.container_articles, .containernav, footer').css({opacity: '0.33'});
      $(this).closest('.article').clone(true).appendTo('.modal');
      $('.modal .article').toggleClass('article_modal');
      $('.detail').toggleClass('nodetail');
      $('.panier').toggleClass('pimppanier');
      $('.modal .article .close').css({display: 'flex'});
      $('.intermodal').css({display: 'flex'});
      $('body').css('overflow-y', 'hidden');
    });

    $('.close').on('click', function() {
      $(this).closest('.article').remove();
      $('.container_articles, .containernav, footer').css({opacity: '1'});
      $('.close').css({display: 'none'});
      $('.detail').toggleClass('nodetail');
      $('.panier').toggleClass('pimppanier');
      $('.intermodal').css({display: 'none'});
      $('body').css('overflow-y', 'visible');
    });

    $('.intermodal').on('click', function() {
      $('.modal').empty();
      $('.container_articles, .containernav, footer').css({opacity: '1'});
      $('.close').css({display: 'none'});
      $('.detail').toggleClass('nodetail');
      $('.panier').toggleClass('pimppanier');
      $('.intermodal').css({display: 'none'});
      $('body').css('overflow-y', 'visible');
    });



    $('.text_article h3').on('click', function () {

      var compteurh3 = $('.text_article h3').length;

      if (compteurh3 == 6){
        $('.container_articles, .containernav, footer').css({opacity: '0.33'});
        $(this).closest('.article').clone(true).appendTo('.modal');
        $('.modal .article').toggleClass('article_modal');
        $('.detail').toggleClass('nodetail');
        $('.panier').toggleClass('pimppanier');
        $('.modal .article .close').css({display: 'flex'});
        $('.intermodal').css({display: 'flex'});
        $('body').css('overflow-y', 'hidden');
      }
    });

    $('.panier').one('click', function() {

      var int = parseInt($('.connexion u').text());
      int++;
      $('.connexion u').html(int);
      $(this).parent().siblings().first().css({display:'block'});
      $(this).remove();
    });


    $('.deco').on("click", function(){
      $('.container_articles, .containernav, footer, .ADMIN, .deco').css({opacity: '0.33'});
      $('.intermodal, .confirm').css({display: 'block'});
      $('body').css('overflow-y', 'hidden');
    });

    $('.intermodal, .non').on('click', function() {
      $('.container_articles, .containernav, footer, .ADMIN, .deco').css({opacity: '1'});
      $('.intermodal,.confirm, .modalsuppr').css({display: 'none'});
      $('body').css('overflow-y', 'visible');
    });

    $('.suppr').on("click", function(){
      $('.container_articles, .containernav, footer, .ADMIN, .deco').css({opacity: '0.33'});
      $('.intermodal, .modalsuppr').css({display: 'block'});
      $('body').css('overflow-y', 'hidden');
      //console.log($(this).attr("id"));
      $("#linkSupp").attr("href", "CRUD/deleteProduct.php?id="+$(this).attr("id"));
    });
  });
