<?php

include "config.php";

$produit = $bdd->prepare("SELECT * FROM `Produit`");
$produit->execute();
$produit_response = $produit->fetchAll();
// var_dump($produit_response);

?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Chez Pupuce</title>
  <link rel="stylesheet" href="pupuce.css">
  <link href="https://fonts.googleapis.com/css?family=Overlock" rel="stylesheet">
  <link rel="shortcut icon" type="image/x-icon" href="img/corgi.png" />
</head>

<body>
  <div class="containernav">
    <div class="barre_menu">
      <div class="titre_logo">
        <h1>Chez Pupuce</h1>
        <img src="img/os.png" alt="Os de chien">
      </div>

      <div class="espace_boutique">
        <h1>Espace boutique</h1>
      </div>

      <div class="connexion">
        <u>0</u>
        <img class="mon_panier" src="img/panier.png" alt="Panier">
        <p><a href="page_login/index.php">S'identifier</a></p>
      </div>
    </div>
  </div>

  <div class="intermodal"></div>

  <div class="modal"></div>

  <div class="container_articles">
    <div class="range_darticle">
      <?php
      foreach($produit_response as $produit){
        ?>
        <div class="article">
          <div class="ajout_panier">
            <p>Produit ajouté au panier!</p>
          </div>
          <img class="close" src="img/fermer.png" alt="Fermer">
          <div class="img_text">
            <div class="img_article">
              <img class="couche" src="<?php echo $produit['Image'] ?>" alt="Couche culottes">
            </div>
            <div class="text_article">
              <h3><u><?php echo $produit['Nom'] ?></u></h3>
              <p><?php echo $produit['Description'] ?></p>
            </div>
            <h3><?php echo $produit['Prix'] ?>€</h3>
          </div>
          <div class="bouton_detail_panier">
            <button class="detail" type="button" name="button">Details:</button>
            <button class="panier" type="button" name="button"><img src="img/panier.png" alt="Panier"></button>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>

  <footer>
    <div class="reseau_part">
      <a href="https://www.facebook.com/"><img src="img/fb.png" alt="Icone Facebook"></a>
      <a href="https://twitter.com/?lang=fr"><img src="img/twitter.png" alt="Icone Twitter"></a>
      <a href="https://www.linkedin.com/feed/"><img src="img/linkedin.png" alt="Icone linkedin"></a>
      <a href="https://www.instagram.com/?hl=fr"><img src="img/insta.png" alt="Icone Insta"></a>
    </div>
    <div class="contacts">
      <p><u>Adresse:</u> 575 rue du Iench</p>
      <p><u>Téléphone:</u> 06.98.76.45.67</p>
    </div>
  </footer>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="script.js">Modal();</script>
</body>
</html>
