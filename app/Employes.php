<?php

//Classe qui sera instanciée
class Employes extends Personne
{

  // Attributs
  private $Num_secu;
  private $Fonction;
  private $Salaire;
  private $Superieur;

  // Constructeur
  function __construct($Nom, $Prenom, $Mail, $Adresse, $Cp, $Ville,  $Date_naissance, $Id, $Num_secu, $Fonction, $Salaire, $Superieur)
  {
    parent::__construct($Nom, $Prenom, $Mail, $Adresse, $Cp, $Ville, $Date_naissance, $Id);

    $this->$Num_secu = $Num_secu;

    $this->$Fonction = $Fonction;

    $this->$Salaire = $Salaire;

    $this->$Superieur = $Superieur;
  }

  // Méthodes

  public function Promotion(){

  }

  public function New()
  {
    // bool
  }

  public function Update()
  {
    // Modif Array : bool
  }

  public function Delete()
  {
    // bool
  }

  public function read()
  {
    // Array
  }
}
