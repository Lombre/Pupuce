<?php
//Class mere abstraite
abstract class Personne
{

  // Attributs
  protected $Nom;
  protected $Prenom;
  protected $Mail;
  protected $Adresse;
  protected $Cp;
  protected $Ville;
  protected $Date_naissance;
  protected $Id;

  // Constructeur
  function __construct($Nom, $Prenom, $Mail, $Adresse, $Cp, $Ville, $Date_naissance, $Id)
  {
    $this->Nom = $Nom;
    $this->Prenom = $Prenom;
    $this->Mail = $Mail;
    $this->Adresse = $Adresse;
    $this->Cp = $Cp;
    $this->Ville = $Ville;
    $this->Date_naissance = $Date_naissance;
    $this->Id = $Id;
  }

  // Methodes
  public function getNom() { return $this->Nom; }
  public function setNom($value) { $this->Nom = $value; }

  public function getPrenom() { return $this->Prenom; }
  public function setPrenom($value) { $this->Prenom = $value; }

  public function getMail() { return $this->Mail; }
  public function setMail($value) { $this->Mail = $value; }

  public function getAdresse() { return $this->Adresse; }
  public function setAdresse($value) { $this->Adresse = $value; }

  public function getCp() { return $this->Cp; }
  public function setCp($value) { $this->Cp = $value; }

  public function getVille() { return $this->Ville; }
  public function setVille($value) { $this->Ville = $value; }

  public function getDate_naissance() { return $this->Date_naissance; }
  public function setDate_naissance($value) { $this->Date_naissance = $value; }

  public function getId() { return $this->Id; }
  public function setId($value) { $this->Id = $value; }

  public function Modif_info($informations){

  };
}
