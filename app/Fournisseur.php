<?php

//Classe qui sera instanciée
class Fournisseur extends Personne
{

  // Attributs
  private $Code_comptable;

  // Constructeur
  function __construct($Nom, $Prenom, $Mail, $Adresse, $Cp, $Ville, $Date_naissance, $Id, $Code_comptable)
  {
    parent::__construct($Nom, $Prenom, $Mail, $Adresse, $Cp, $Ville, $Date_naissance, $Id);

    $this->$Code_comptable = $Code_comptable;
  }

  // Méthodes
  public function New()
  {
    // bool
  }

  public function Read()
  {
    // Array
  }

  public function Update()
  {
    // bool
  }

  public function Delete()
  {
    // bool
  }
}
