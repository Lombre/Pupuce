<?php

//Classe qui sera instanciée
class Clients extends Personne
{

  // Attributs
  private $Date_inscription;
  private $Mdp;

  // Constructeur
  function __construct($Nom, $Prenom, $Mail, $Adresse, $Cp, $Ville, $Date_naissance, $Id, $Date_inscription, $Mdp)
  {
    parent::__construct($Nom, $Prenom, $Mail, $Adresse, $Cp, $Ville, $Date_naissance, $Id);

    $this->$Date_inscription = $Date_inscription;

    $this->$Mdp = $Mdp;
  }

  // Méthodes

  public function getDate_inscription() { return $this->Date_inscription; }
  public function setDate_inscription($value) { $this->Date_inscription = $value; }

  public function New(){
    // bool
  }

  public function read()
  {
    // Array
  }

  public function Update()
  {
    // bool
  }

  public function Delete()
  {
    // bool
  }

  public function Authentification()
  {
    // bool
  }
}
