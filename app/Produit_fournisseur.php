<?php

class Produit_fournisseur
{
  // Attributs

  protected $Fournisseur_id;
  protected $Produit_id;
  protected $Quantite;

  // Constructeur

  function __construct($Fournisseur_id, $Produit_id, $Quantite)
  {
    $this->Fournisseur_id = $Fournisseur_id;
    $this->Produit_id = $Produit_id;
    $this->Quantite = $Quantite;
  }
}
