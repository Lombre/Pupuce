<?php

class Produit
{
  // Attributs

  protected $Id;
  protected $Nom;
  protected $Description;
  protected $Image;
  protected $Prix;
  protected $Quantite_stock;


  // Constructeur

  function __construct($Id, $Nom, $Description, $Image, $Prix, $Quantite_stock)
  {
    $this->Id = $Id;
    $this->Nom = $Nom;
    $this->Description = $Description;
    $this->Image = $Image;
    $this->Prix = $Prix;
    $this->Quantite_stock = $Quantite_stock;
  }

  // Methodes

  public function getInfo()
  {
    // Array
  }

  public function UpdateProduit()
  {
    // bool
  }

  public function DeleteProduit()
  {
    // bool
  }

  public function CreateProduit()
  {
    // bool
  }
}
