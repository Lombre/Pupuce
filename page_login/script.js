$( document ).ready(function(){


    $(".input").on("change", function()
{
    if ($(this).val().length < 3)
    {
        $(this).css("color", "#B10000");
    }
    else
    {
        $(this).css("background-color", "white");

    }
});

    $("#submit").on("click", function(e)
    {
        if ( ($("#nick").val().length < 3) || ($("#pwd").val().length < 3) )
        {
            e.preventDefault();
            $(".input").trigger("change");
            var errorText = $("<p></p>").text("L'identifiant et le mot de passe doivent faire au moins 3 caractères.");
            errorText.attr('id', 'error');
            $('fieldset p').remove();
            $("fieldset").append(errorText);
        }
        else
        {
            $("fieldset").remove("#error");
            return true;
        }
    });

    $('.inscription').on('click', function() {
      $('#myForm').css({display: 'none'});
      $('#inscri').css({display: 'block'});
    })
});
