<!DOCTYPE html>
<?php //echo password_hash("lalala", PASSWORD_DEFAULT);?>
<!-- password_verify('lalala', ) -->
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Connexion</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="shortcut icon" type="image/x-icon" href="../img/corgi.png" />
</head>

<body>

  <?php

  //Variable contenant le code du formulaire
  $formulaire=<<<'FIN'
  <div class="titre">
  <h1>Pupuce<h1>
  </div>
  <img class="PUPUCE" src="../img/os.png">
  <a href="../pupuce.php"><img class="retour" src="retour.png" alt="Flêche de retour"></a>
  <form id="myForm" name="myForm" action="index.php" method="POST">
  <fieldset id="connect">
  <label>Identifiant:</label> <br>
  <input type="text" id="nick" name="nick" class="input" required pattern=".{3,}"> <br>
  <label>Mot de passe:</label> <br>
  <input type="password" id="pwd" name="pwd" class="input" required pattern=".{3,}"> <br>
  <input class="boubou" type="submit" id="submit" name="submit" value="Connexion"> <br>
  <p class="inscription">S'inscrire?</p>
  </fieldset>
  </form>
  <form id="inscri">
  <label>Choisir un Identifiant:</label> <br>
  <input type="text" id="identifiant" name="identifiant" class="input" required pattern=".{3,}"> <br>
  <label>Choisir un Mot de passe:</label> <br>
  <input type="password" id="mdp" name="mdp" class="input" required pattern=".{3,}"> <br>
  <input class="boubou" type="submit" id="envoi" name="submit" value="S'inscrire!"> <br>
  </form>
   <br>
FIN;




  if ( (isset($_POST['nick']) && !empty($_POST['nick'])) && (isset($_POST['pwd']) && !empty($_POST['pwd'])) ) { //On vérifie que les variables existe et non vide

    //On sécurise les inputs
    $_POST['nick'] = htmlspecialchars($_POST['nick']);
    $_POST['pwd'] = htmlspecialchars($_POST['pwd']);


    //Charge le fichier json et le transforme en tableau associatif
    $content = file_get_contents('identifiants.json', FILE_USE_INCLUDE_PATH);
    $json = json_decode($content , true);

    $connection = false;

    foreach ($json as $key) { //On itére sur le tableau json
      if (strtolower($_POST['nick']) == strtolower( $key['identifiant'] ) ) { //On vérifie si l'Identifiant rentré est le même que le json
        if (password_verify($_POST["pwd"], $key["crypted"])) { //On vérifie que le mdp soit les mêmes
          $connection = true; //On se connecte
          break; //On sort de la boucle
        }
      }
    }

    //On affiche un message de bienvenue
    if ($connection) {
      header('Location:../pupuce_admin.php');
      writeInLogs(true); //On écrit dans les logs
    }
    //Sinon on affiche a nouveau le formulaire et un message erreur
    else {
      echo $formulaire;
      echo '<p style="color: red; margin-top: 20px;">Identifiant ou mot de passe incorrect</p>';
      writeInLogs(false); //On écrit dans les logs
    }
  }

  else {
    echo $formulaire; //Si les variables existent pas ou sont vide on affiche juste le formulaire
  }


  //Écrit dans le fichier de logs
  function writeInLogs($valeur)
  {
    date_default_timezone_set('Europe/Paris');
    $handle = fopen("logs.txt", "a");
    $id = 'Nickname : ' . $_POST['nick'] . ' Password : ' . $_POST['pwd'];
    $ip = get_ip();

    $date = date("c");
    if ($valeur)
    {
      fwrite($handle, $id . ' ' . $ip . ' ' . $date . " Connexion réussi. " . PHP_EOL );
    } else
    {
      fwrite($handle, $id . ' ' . $ip . ' ' . $date . " Connexion échoué. " . PHP_EOL );
    }
    fclose($handle);
  }

  function get_ip() {
    // IP si internet partagé
    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
      return $_SERVER['HTTP_CLIENT_IP'];
    }
    // IP derrière un proxy
    elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      return $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    // Sinon : IP normale
    else {
      return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
    }
  }

  ?>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
  <script type="text/javascript" src="script.js">

  </script>
</body>
</html>
